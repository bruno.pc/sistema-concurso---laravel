<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/Jurados', 'Jurado@listarJurados')->name('jurado');
Route::get('/Jurados/RegistrarJurado', function(){
	return view('registrarJurado');
})->name('jurado.registrarJurado');
Route::post('/Jurados/RegistrarJurado/Confirmar', 'Jurado@registrarJurado')->name('jurado.registrarJurado.confirmar');
Route::get('/Participante/MinhaFicha', 'Ficha@mostrarFicha')->name('participante.ficha');
Route::post('/Participante/MinhaFicha/Criar', 'Ficha@criarFicha')->name('participante.ficha.criar');
Route::get("/Concurso", 'Concurso@listarConcursosAtivos')->name("concurso");
Route::get('/Concurso/CriarConcurso', function(){
	return view('criarConcurso');
})->name('concurso.criarConcurso');
Route::post("/Concurso/CriarConcurso/Confirmar", "Concurso@criarConcurso")->name("concurso.criarConcurso.confirmar");
Route::get("/Concurso/AdicionarJurado/{concursoId}", "ConcursoJurado@listarJurados")->name("concurso.adicionarJurado");
Route::get("/Concurso/AdicionarJurado/{concursoId}/{juradoId}", "ConcursoJurado@criarConcursoJurado")->name("concurso.adicionarJurado.confirmar");
Route::get("/Concurso/ClassificarParticipante/{concursoId}", "ConcursoFicha@listarFicha")->name("concurso.classificarParticipante");
Route::get("/Concurso/ClassificarParticipante/{concursoId}/{fichaId}", "ConcursoFicha@classificarParticipante")->name("concurso.classificarParticipante.confirmar");
Route::get("/Concurso/Inscrever/{concursoIdd}/{usuarioId}", "ConcursoFicha@inscreverParticipante")->name("concurso.inscreverParticipante");
Route::get("/Concurso/Participantes/{concursoId}", "ConcursoFicha@listarFichaJurado")->name("concurso.participantes");
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');