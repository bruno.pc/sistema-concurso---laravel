@extends('layouts.app')
@section('header')
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/viacep.js') }}" ></script>
    <link rel='stylesheet' href="{{ asset('css/css.css') }}" />
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Sua ficha') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('participante.ficha.criar') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="dataNascimento" class="col-md-4 col-form-label text-md-right">
                                Data de Nascimento: 
                            </label>
                            <div class="col-md-6">
                                <input id="dataNascimento" type="date" class="form-control" name="dataNascimento" required>
                            </div>
                        </div>                        
                        <div class="form-group row">
                            <label for="instrumento" class="col-md-4 col-form-label text-md-right">
                                Instrumento para concurso: 
                            </label>
                            <div class="col-md-6">
                                <input id="instrumento" type="text" class="form-control" name="instrumento" required>
                            </div>
                        </div>                       
                        <div class="form-group row">
                            <label for="youtube" class="col-md-4 col-form-label text-md-right">
                                Link do YouTube: 
                            </label>
                            <div class="col-md-6">
                                <input id="youtube" type="text" class="form-control" name="youtube" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="mensagem" class="col-md-4 col-form-label text-md-right">
                                Mensagem aos Jurados: 
                            </label>
                            <div class="col-md-6">
                                <input id="mensagem" type="text" class="form-control" name="mensagem">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fone_user" class="col-md-4 col-form-label text-md-right">
                                Telefone: 
                            </label>
                            <div class="col-md-6">
                                <input id="fone_user" type="text" class="form-control" name="fone_user" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cep" class="col-md-4 col-form-label text-md-right">
                                CEP: 
                            </label>
                            <div class="col-md-6">
                                <input id="cep" type="text" class="form-control" name="cep" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="estado" class="col-md-4 col-form-label text-md-right">
                                Estado: 
                            </label>
                            <div class="col-md-6">
                                <input id="estado" type="text" class="form-control" name="estado" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cidade" class="col-md-4 col-form-label text-md-right">
                                Cidade:
                            </label>
                            <div class="col-md-6">
                                <input id="cidade" type="text" class="form-control" name="cidade" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="bairro" class="col-md-4 col-form-label text-md-right">
                                Bairro: 
                            </label>
                            <div class="col-md-6">
                                <input id="bairro" type="text" class="form-control" name="bairro" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rua" class="col-md-4 col-form-label text-md-right">
                                Rua: 
                            </label>
                            <div class="col-md-6">
                                <input id="rua" type="text" class="form-control" name="rua" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="numero" class="col-md-4 col-form-label text-md-right">
                                Número: 
                            </label>
                            <div class="col-md-6">
                                <input id="numero" type="number" class="form-control" name="numero" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="complemento" class="col-md-4 col-form-label text-md-right">
                                Complemento: 
                            </label>
                            <div class="col-md-6">
                                <input id="complemento" type="text" class="form-control" name="complemento" required>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Criar Ficha') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
