@extends('layouts.app')
@section('content')
	<br><br>
	<h1>Jurados disponíveis</h1>
	<table border='1'>
		<thead>
			<tr>
				<td>Nome do Jurado</td>
				<td>Email</td>
				<td>Adicionar</td>
			</tr>
		</thead>
		<tbody>
			@foreach($juradosAdd as $jurado)
			<tr>
				<td>{{ $jurado->getNome() }}</td>
				<td>{{ $jurado->getEmail() }}</td>
				<td><a href="{{ route('concurso.adicionarJurado.confirmar', [$concursoId, $jurado->getId()]) }}">Adicionar</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<br><br>
	<h1>Jurados presentes no concurso</h1>
	<table border='1'>
		<thead>
			<tr>
				<td>Nome do Jurado</td>
				<td>Email</td>
			</tr>
		</thead>
		<tbody>
			@foreach($juradosAdicionados as $jurado)
			<tr>
				<td>{{ $jurado->getNome() }}</td>
				<td>{{$jurado->getEmail()}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@endsection