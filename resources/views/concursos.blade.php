@extends('layouts.app')
@section('content')
	@if(\Auth::User()->getNivel() == 3)
		<a href="{{ route('concurso.criarConcurso') }}">
			<button>Criar um concurso</button>
		</a>
		<br><br>
		<form action="{{ route('concurso') }}" method='get'>
			<label for='search'>Pesquisar: </label><input id='search' type='text' name='search'>
		</form>
		<br><br>
		<table border='1'>
			<thead>
				<tr>
					<td>Nome do Concurso</td>
					<td>Data Final das Inscrições</td>
					<td>Data da Final</td>
					<td>Jurados</td>
					<td>Participantes</td>

				</tr>
			</thead>
			<tbody>
				@foreach($concursos as $item)
				<tr>
					<td>{{$item->getNome()}}</td>
					<td>{{$item->getDataFinalInscricao()}}</td>
					<td>{{$item->getDataFinalConcurso()}}</td>
					<td><a href="{{ route('concurso.adicionarJurado', $item->getId()) }}">Adicionar Jurado</a></td>
					<td><a href="{{ route('concurso.classificarParticipante', $item->getId()) }}">Classificar aluno</a></td> 
				</tr>
				@endforeach
			</tbody>
		</table>
	@elseif(\Auth::User()->getNivel() == 1)
		<h1>Concursos Disponíveis</h1>
		<table border='1'>
			<thead>
				<tr>
					<td>Nome do Concurso</td>
					<td>Data Final das Inscrições</td>
					<td>Data da Final</td>
				</tr>
			</thead>
			<tbody>
				@foreach($concursoInscrever as $item)
				<tr>
					<td>{{$item->getNome()}}</td>
					<td>{{$item->getDataFinalInscricao()}}</td>
					<td>{{$item->getDataFinalConcurso()}}</td>
					<td><a href="{{ route('concurso.inscreverParticipante', [$item->getId(), $fichaId]) }}">Inscrever-se</a></td>
				@endforeach
				</tr>
			</tbody>
		</table>
		<br><br>
		<h1>Concursos Já Inscritos</h1>
		<table border='1'>
			<thead>
				<tr>
					<td>Nome do Concurso</td>
					<td>Data Final das Inscrições</td>
					<td>Data da Final</td>
				</tr>
			</thead>
			<tbody>
				@foreach($concursoInscrito as $item)
				<tr>
					<td>{{$item->getNome()}}</td>
					<td>{{$item->getDataFinalInscricao()}}</td>
					<td>{{$item->getDataFinalConcurso()}}</td>
				@endforeach
				</tr>
			</tbody>
		</table>
	@elseif(\Auth::User()->getNivel() == 2)
		<h1>Concursos Para votar</h1>
		<table border='1'>
			<thead>
				<tr>
					<td>Nome do Concurso</td>
					<td>Data da Final</td>
				</tr>
			</thead>
			<tbody>
				@foreach($concursosVotar as $item)
				<tr>
					<td>{{$item->getNome()}}</td>
					<td>{{$item->getDataFinalInscricao()}}</td>
					<td>{{$item->getDataFinalConcurso()}}</td>
					<td><a href="{{ route('concurso.participantes', $item->getId()) }}">Votar nos Participantes</a></td>
				@endforeach
				</tr>
			</tbody>
		</table>
	@endif
@endsection