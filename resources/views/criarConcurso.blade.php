@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Concurso') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('concurso.criarConcurso.confirmar') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="nome" class="col-md-4 col-form-label text-md-right">{{ __('Nome do Concurso') }}</label>
                            <div class="col-md-6">
                                <input type='text' id="nome" class="form-control" name="nome" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dataFinalInscricao" class="col-md-4 col-form-label text-md-right">{{ __('Data do Fim das Inscrições') }}</label>
                            <div class="col-md-6">
                                <input type='date' id="dataFinalInscricao" class="form-control" name="dataFinalInscricao" required>
                            </div>
                        </div>
		                <div class="form-group row">
                            <label for="dataFinalConcurso" class="col-md-4 col-form-label text-md-right">{{ __('Data da Final') }}</label>
                            <div class="col-md-6">
                                <input type='date' id="dataFinalConcurso" class="form-control" name="dataFinalConcurso" required>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Criar Concurso') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection