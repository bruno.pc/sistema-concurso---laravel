@extends('layouts.app')
@section('header')
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/viacep.js') }}" ></script>
@endsection
@section('content')
	<a href="{{ route('jurado.registrarJurado') }}">
		<button>Criar um jurado</button>
	</a>
	<br><br>
	<form action="{{ route('jurado') }}" method='get'>
		<label for='search'>Pesquisar: </label><input id='search' type='text' name='search'>
	</form>
	<br><br>
	<table border='1'>
		<thead>
			<tr>
				<td>Nome</td>
				<td>Email</td>
				<td>Fone</td>
			</tr>
		</thead>
		<tbody>
	@foreach($jurados as $jurado)
			<tr>
				<td>{{ $jurado->getNome() }}</td>
				<td>{{ $jurado->getEmail() }}</td>
				<td>{{ $jurado->getFone() }}</td>
			</tr>
	@endforeach
		</tbody>
@endsection