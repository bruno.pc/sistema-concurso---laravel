@extends('layouts.app')
@section('content')
	<br><br>
	<h1>Participante disponíveis</h1>
	<table border='1'>
		<thead>
			<tr>
				<td>Nome do Participante</td>
				<td>Email</td>
				<td>Link do Vídeo</td>
			</tr>
		</thead>
		<tbody>
			@foreach($fichasAdd as $ficha)
			<tr>
				<td>{{ $ficha->getParticipante->getNome() }}</td>
				<td>{{ $ficha->getParticipante->getEmail() }}</td>
				<td>{{ $ficha->getYoutube() }}</td>
				<td><a href="{{ route('concurso.classificarParticipante.confirmar', [$concursoId, $ficha->getId()]) }}">Classificar</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<br><br>
	<h1>Participantes classificados</h1>
	<table border='1'>
		<thead>
			<tr>
				<td>Nome do Participante</td>
				<td>Email</td>
				<td>Link do Vídeo</td>
			</tr>
		</thead>
		<tbody>
			@foreach($fichasAdicionadas as $ficha)
			<tr>
				<td>{{ $ficha->getParticipante->getNome() }}</td>
				<td>{{ $ficha->getParticipante->getEmail() }}</td>
				<td>{{ $ficha->getYoutube() }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@endsection