<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("ficha_id");
            $table->integer("concurso_id");
            $table->integer("jurado_id");
            $table->float("fidelidadeEstilo")->nullable();
            $table->float("qualidade")->nullable();
            $table->float("dificuldade")->nullable();
            $table->float("leituraPartitura")->nullable();
            $table->float("sonoridade")->nullable();
            $table->float("presencaPalco")->nullable();
            $table->float("precisaoExecucao")->nullable();
            $table->float("musicalidade")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notas');
    }
}
