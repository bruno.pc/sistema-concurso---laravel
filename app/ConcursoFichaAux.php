<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConcursoFichaAux extends Model
{
    protected $fillable = [
    	'ficha_id', 'concurso_id', 'status'
    ];
    public function getId(){return $this->id;}
    public function getFicha(){return $this->belongsTo('\App\Ficha', 'ficha_id');}
    public function getConcurso(){return $this->belongsTo('\App\Concurso', 'concurso_id');}
    public function getStatus(){return $this->status;}
    public function getFichaId(){return $this->ficha_id;}
}
