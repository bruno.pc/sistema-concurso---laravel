<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ficha extends Model
{
    protected $fillable = [
    	'dataNascimento', 'instrumento', 'youtube', 'mensagem', 'cep', 'rua', 'numero', 'complemento', 'bairro', 'estado', 'cidade', 'participante_id'
    ];

    public function getId(){return $this->id;}
    public function getNascimento(){return $this->dataNascimento;}
    public function getInstrumento(){return $this->instrumento;}
    public function getYoutube(){return $this->youtube;}
    public function getMensagem(){return $this->mensagem;}
    public function getCep(){return $this->cep;}
    public function getRua(){return $this->rua;}
    public function getNumero(){return $this->numero;}
    public function getComplemento(){return $this->complemento;}
    public function getBairro(){return $this->bairro;}
    public function getEstado(){return $this->estado;}
    public function getCidade(){return $this->cidade;}
    public function getParticipante(){return $this->belongsTo('\App\User', 'participante_id');}
}
