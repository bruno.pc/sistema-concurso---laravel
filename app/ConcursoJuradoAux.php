<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConcursoJuradoAux extends Model
{
    protected $fillable = [
    	'concurso_id', 'jurado_id'
    ];
    public function getId(){return $this->id;}
    public function getJurado(){return $this->belongsTo("\App\User", "jurado_id");}
    public function getConcurso(){return $this->belongsTo("\App\Concurso", "concurso_id");}
}
