<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Jurado extends Controller
{
    public function registrarJurado(Request $request){
    	\App\User::create([
    		'name' => $request->name,
    		'email' => $request->email,
    		'password' => bcrypt($request->password),
    		'nivel_user' => 2,
    		'fone_user' => $request->fone_user,
    	]);
    	return redirect()->route('jurado');
    }
    public function listarJurados(Request $request){
    	if($request->search == null){
    		$jurados = \App\User::where('nivel_user', 2)->get();
    	}else{
    		$jurados = \App\User::where('nivel_user', 2)->where('name', 'like', "%" . $request->search . "%")->get();
    	}
    	return view('jurados', compact('jurados'));
    }
}
