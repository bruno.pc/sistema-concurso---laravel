<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Ficha extends Controller
{
	public function mostrarFicha(){
		$id = \Auth::User()->getId();
		if(\Auth::User()->getFicha->last() != null){
			$ficha = \Auth::User()->getFicha->last();
			return view('ficha', compact('ficha'));
		}else{
			return view('criarFicha');
		}
	}
	public function criarFicha(Request $request){
		$id = \Auth::User()->getId();
		\App\Ficha::create([
			'dataNascimento' => $request->dataNascimento,
			'instrumento' => $request->instrumento,
			'youtube' => $request->youtube,
			'mensagem' => $request->mensagem,
			'cep' => $request->cep,
			'estado' => $request->estado,
			'cidade' => $request->cidade,
			'bairro' => $request->bairro,
			'rua' => $request->rua,
			'numero' => $request->numero,
			'complemento' => $request->complemento,
			'participante_id' => $id
		]);
		\App\User::where('id', $id)->update(['fone_user' => $request->fone_user]);
		return view("home");
	}
}

