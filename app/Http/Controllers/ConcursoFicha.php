<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConcursoFicha extends Controller
{
    public function listarFicha($concursoId){
    	$fichas = \App\ConcursoFichaAux::where(['concurso_id' => $concursoId, 'status' => 1])->get();
    	$fichasAdd = [];
    	foreach($fichas as $ficha){
			$ficha = \App\Ficha::find($ficha->getFichaId());
			array_push($fichasAdd, $ficha);
    	}
    	$fichas = \App\ConcursoFichaAux::where(['concurso_id' => $concursoId, 'status' => 2])->get();
    	$fichasAdicionadas = [];
    	foreach($fichas as $ficha){
    		$ficha = \App\Ficha::find($ficha->getFichaId());
    		array_push($fichasAdicionadas, $ficha);
    	}
    	return view("adicionarFicha", compact("fichasAdd", "fichasAdicionadas" , "concursoId"));
    }
    public function listarFichaJurado($concursoId){
      $fichas = \App\ConcursoFichaAux::where(['concurso_id' => $concursoId, 'status' => 2])->get();
      $fichasVotar = [];
      foreach($fichas as $ficha){
        $ficha = \App\Ficha::find($ficha->getFichaId());
        array_push($fichasVotar, $ficha);
      }
      return view("concursoParticipantes", compact('concursoId', 'fichasVotar'));
    }
    public function classificarParticipante($concursoId, $fichaId){
   		\App\ConcursoFichaAux::where('ficha_id', $fichaId)->update(['status' => 2]);
   		return back();
   	}
   	public function inscreverParticipante($concursoId, $fichaId){
   		\App\ConcursoFichaAux::create(['ficha_id' => $fichaId, 'concurso_id' => $concursoId, 'status' => 1]);
   		return back();
   	}
}