<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConcursoJurado extends Controller
{
    public function listarJurados($concursoId){
    	$jurados = \App\User::where(["nivel_user" => 2])->get();
    	$juradosAdd = [];
    	$juradosAdicionados = [];
    	foreach($jurados as $jurado){
    		$verificacao = \App\ConcursoJuradoAux::where("concurso_id", $concursoId)->where("jurado_id", $jurado->getId())->exists();
    		if(!$verificacao){
    			array_push($juradosAdd, $jurado);
    		}else{
    			array_push($juradosAdicionados, $jurado);
    		}
    	}
    	return view("adicionarJurado", compact("juradosAdd", "juradosAdicionados" , "concursoId"));
    }
   	public function criarConcursoJurado($concursoId, $juradoId){
   		\App\ConcursoJuradoAux::create([
   		'concurso_id' => $concursoId,
   		'jurado_id' => $juradoId,
   		]);
   		return back();
   	}
}