<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Nota extends Controller
{
    public function criarNota($concursoId, $fichaId, Request $request){
    	$request->merge(['concurso_id' => $concursoId, 'ficha_id' => $fichaId, 'jurado_id' => \Auth::User()->getId()]);
    	\App\Nota::create($request->all());
    }
}
