<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Concurso extends Controller
{
	public function criarConcurso(Request $request){
        $request->merge(['status' => 1]);
		\App\Concurso::create($request->all());
        return redirect()->route('concurso');
	}
	public function listarConcursosAtivos(Request $request){
    	if(\Auth::User()->getNivel()==3){
            if($request->search == null){
                $concursos = \App\Concurso::where('status', "!=", 4)->get();
            }else{
                $concursos = \App\Concurso::where('status', "!=", 4)->where('nome', 'like', "%" . $request->search . "%")->get();
                return view('concursos', compact('concursos'));    
            }
        }else if(\Auth::User()->getNivel() == 1){
            $fichas = \App\Ficha::where('participante_id', \Auth::User()->getId())->get();
            $concursos = \App\Concurso::where('status', "!=", 4)->get();
            $concursoInscrito = [];
            $concursoInscrever = [];
            foreach($concursos as $concurso){
                $inscrito = false;
                foreach($fichas as $ficha){
                    $verificacao = \App\ConcursoFichaAux::where(['concurso_id' => $concurso->getId(), 'ficha_id' => $ficha->getId()])->exists();
                    if($verificacao){
                        $inscrito = true;
                    }
                }
                if($inscrito){
                    array_push($concursoInscrito, $concurso);
                }else{
                    array_push($concursoInscrever, $concurso);
                }
            }
            $ficha = \Auth::User()->getFicha->last();
            $fichaId = $ficha->getId();
            return view('concursos', compact('fichaId', 'concursoInscrever', 'concursoInscrito'));
        }else if(\Auth::User()->getNivel() ==2){
            $concursos = \App\Concurso::where('status', "!=", 4)->get();
            $concursosVotar = [];
            foreach($concursos as $concurso){
                $verificacao = \App\ConcursoJuradoAux::where(['concurso_id' => $concurso->getId(), 'jurado_id' => \Auth::User()->getId()])->exists();
                if($verificacao){
                    array_push($concursosVotar, $concurso);
                }
            }
            return view('concursos', compact('concursosVotar'));
        }
    }
}
