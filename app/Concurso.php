<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concurso extends Model
{
    protected $fillable = [
    	'nome', 'vencedor', 'dataFinalInscricao', 'dataFinalConcurso', 'status'
    ];
    public function getId(){return $this->id;}
    public function getNome(){return $this->nome;}
    public function getVencedor(){return $this->vencedor;}
    public function getDataFinalInscricao(){return $this->dataFinalInscricao;}
    public function getDataFinalConcurso(){return $this->dataFinalConcurso;}
    public function getStatus(){return $this->status;}
}
