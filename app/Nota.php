<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $fillable = [
    	'ficha_id', 'concurso_id', 'jurado_id', 'fidelidadeEstilo', 'qualidade', 'dificuldade', 'leituraPartitura', 'sonoridade', 'presencaPalco', 'precisaoExecucao', 'musicalidade'
    ];
    public function getId(){return $this->id;}
    public function getFicha(){return $this->belongsTo("App\Ficha", "ficha_id");}
    public function getConcurso(){return $this->belongsTo("App\Concurso", "concurso_id");}
    public function getJurado(){return $this->belongsTo("App\User", "jurado_id");}
    public function getFidelidadeEstilo(){return $this->fidelidadeEstilo;}
    public function getQualidade(){return $this->qualidade;}
    public function getDificuldade(){return $this->dificuldade;}
    public function getLeituraPartitura(){return $this->leituraPartitura;}
    public function getSonoridade(){return $this->sonoridade;}
    public function getPresencaPalco(){return $this->presencaPalco;}
    public function getPrecisaoExecucao(){return $this->precisaoExecucao;}
    public function getMusicalidade(){return $this->musicalidade;}
}
